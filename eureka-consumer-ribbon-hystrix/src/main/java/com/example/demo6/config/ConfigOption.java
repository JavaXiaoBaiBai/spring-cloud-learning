package com.example.demo6.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

@Configuration
public class ConfigOption {
    @Bean
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }
}
