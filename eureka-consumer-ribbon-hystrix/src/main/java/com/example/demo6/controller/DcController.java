package com.example.demo6.controller;

import com.example.demo6.service.DemoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class DcController {
    @Autowired
    DemoService demoService;

    @GetMapping("/dc")
    public String dc() {
        return demoService.consumer();
    }
}
