package com.example.demo6.service.impl;

import com.example.demo6.service.DemoService;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class DemoServiceImpl implements DemoService {
    @Autowired
    RestTemplate restTemplate;

    @Override
    @HystrixCommand(fallbackMethod = "fallBack")
    public String consumer() {
        return restTemplate.getForObject("http://localhost:8520/eureka-client/dc", String.class);
    }

    @Override
    public String fallBack() {
        return "fallBack";
    }
}
