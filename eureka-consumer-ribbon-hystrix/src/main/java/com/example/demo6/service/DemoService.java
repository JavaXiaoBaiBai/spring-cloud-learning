package com.example.demo6.service;

public interface DemoService {
    String consumer();

    String fallBack();
}
