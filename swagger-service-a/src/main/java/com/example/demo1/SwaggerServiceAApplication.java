package com.example.demo1;

import com.spring4all.swagger.EnableSwagger2Doc;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@EnableDiscoveryClient
@EnableSwagger2Doc
@SpringBootApplication
public class SwaggerServiceAApplication {

    public static void main(String[] args) {
        SpringApplication.run(SwaggerServiceAApplication.class, args);
    }

}
