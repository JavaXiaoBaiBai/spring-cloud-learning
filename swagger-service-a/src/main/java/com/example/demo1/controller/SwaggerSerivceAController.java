package com.example.demo1.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import springfox.documentation.annotations.ApiIgnore;

@RestController
public class SwaggerSerivceAController {

    private final Logger logger = LoggerFactory.getLogger(SwaggerSerivceAController.class);

    @Autowired
    DiscoveryClient discoveryClient;

    @Autowired
    RestTemplate restTemplate;

    @GetMapping("/service-a")
    public String dc(String name) {
        String service = "Services:[" + name + "] " + discoveryClient.getServices();
        System.out.println(service);
        System.out.println(name);
        return service;
    }

    @GetMapping("/trace-1")
    public String traceOne() {
        logger.info("==<call trace-1>==");
        logger.info("===<call trace-1>===");
        logger.info( dc("zhangsan"));

        return restTemplate.getForEntity("http://swagger-service-b/trace-2", String.class).getBody();
    }
}
