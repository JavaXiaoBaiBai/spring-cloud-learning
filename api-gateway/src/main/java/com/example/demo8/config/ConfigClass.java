package com.example.demo8.config;

import com.example.demo8.filter.AccessFilter;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

@Component
public class ConfigClass {
    @Bean
    public AccessFilter accessFilter() {
        return new AccessFilter();
    }
}
