package com.example.demo3.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.loadbalancer.LoadBalancerClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
public class DcController {

    @Autowired
    LoadBalancerClient loadBalancerClient;

    @Autowired
    RestTemplate restTemplate;

    @GetMapping("/dc")
    public String dc() {
        ServiceInstance choose = loadBalancerClient.choose("eureka-client");
        String url = "http://" + choose.getHost() + ":" + choose.getPort() + "/dc";
        System.out.println("URL: " + url);
        return restTemplate.getForObject(url, String.class);
    }
}
