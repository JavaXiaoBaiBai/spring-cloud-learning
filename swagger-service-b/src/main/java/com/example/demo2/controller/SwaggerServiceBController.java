package com.example.demo2.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SwaggerServiceBController {
    private Logger logger = LoggerFactory.getLogger(SwaggerServiceBController.class);

    @Autowired
    DiscoveryClient discoveryClient;

    @GetMapping("/service-b")
    public String dc() {
        String services = "Services: " + discoveryClient.getServices();
        System.out.println(services);
        return services;
    }

    @GetMapping("/trace-2")
    public String traceTwo() {
        logger.info("==<call trace-2>==");
        return "Hello World";
    }
}
