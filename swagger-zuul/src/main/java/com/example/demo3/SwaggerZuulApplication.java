package com.example.demo3;

import com.spring4all.swagger.EnableSwagger2Doc;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;

@EnableZuulProxy
@EnableSwagger2Doc
@SpringBootApplication
public class SwaggerZuulApplication {

    public static void main(String[] args) {
        SpringApplication.run(SwaggerZuulApplication.class, args);
    }

}
