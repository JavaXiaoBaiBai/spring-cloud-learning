package com.example.demo4;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.config.server.EnableConfigServer;

@EnableConfigServer
@SpringBootApplication
public class ConfigCenterGiteeApplication {

    public static void main(String[] args) {
        SpringApplication.run(ConfigCenterGiteeApplication.class, args);
    }

}
