package com.example.demo5.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class DcController {
    @Value("${info.profile}")
    public String info;
    @GetMapping("/info")
    public String makeSureImRun() {
        return info;
    }
}
